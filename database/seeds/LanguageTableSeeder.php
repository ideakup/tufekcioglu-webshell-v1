<?php

use Illuminate\Database\Seeder;

class LanguageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('language')->delete();

        DB::table('language')->insert([
			'name' => 'Türkçe',
			'code' => 'tr',
			'flag_url' => null,
			'order' => '1',
			'status' => 'active',
			'deleted' => 'no'
        ]);

        DB::table('language')->insert([
			'name' => 'İngilizce',
			'code' => 'en',
			'flag_url' => null,
			'order' => '2',
			'status' => 'active',
			'deleted' => 'no'
        ]);

        DB::table('language')->insert([
			'name' => 'Almanca',
			'code' => 'de',
			'flag_url' => null,
			'order' => '3',
			'status' => 'active',
			'deleted' => 'no'
        ]);

        DB::table('language')->insert([
			'name' => 'Rusça',
			'code' => 'ru',
			'flag_url' => null,
			'order' => '4',
			'status' => 'active',
			'deleted' => 'no'
        ]);

        DB::table('language')->insert([
			'name' => 'Chinese',
			'code' => 'zh',
			'flag_url' => null,
			'order' => '5',
			'status' => 'active',
			'deleted' => 'no'
        ]);

        DB::table('language')->insert([
			'name' => 'Arabic',
			'code' => 'ar',
			'flag_url' => null,
			'order' => '6',
			'status' => 'active',
			'deleted' => 'no'
        ]);


    }
}