<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	//$this->call(PermissionTableSeeder::class);
    	//$this->call(RoleTableSeeder::class);

        $this->call(SiteSettingsTableSeeder::class);
        $this->call(LanguageTableSeeder::class);
        //$this->call(MenuTableSeeder::class);

    }
}
