<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhotoGallery extends Model
{
    protected $table = 'photogallery';

    public function content()
    {
        return $this->belongsTo('App\Content', 'id', 'content_id');
    }
   
}
