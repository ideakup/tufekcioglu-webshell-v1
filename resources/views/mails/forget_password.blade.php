@component('mail::message')
# Merhaba {{$data['name']}}

Şifrenizi yenilemek için talebinize karşılık bu e-posta size ulaştırılmıştır. Aşağıdaki adrese tıklayarak şifrenizi yenileyebilirsiniz.

<a href='{{ url('/password/reset/'.$token) }}' target='_blank'>Şifrenizi Yenilemek İçin Tıklayınız...</a>

Teşekkürler,<br>
{{ config('app.name') }}
@endcomponent
