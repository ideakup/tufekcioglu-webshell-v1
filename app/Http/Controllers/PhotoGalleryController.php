<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Validator;

use App\Language;
use App\User;
use App\Menu;
use App\MenuVariable;
use App\Content;
use App\ContentVariable;
use App\PhotoGallery;
use App\Slider;

use App;

class PhotoGalleryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

	public function list()
    {
        $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
        return view('photogallery.list', array('langs' => $langs));
    }

}
