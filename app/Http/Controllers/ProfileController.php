<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Validator;

use App\User;


class ProfileController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('profile');
    }

    public function change_password(Request $request)
    {
        $rules = array(
            'oldPassword' => 'required|string|min:6',
            'newPassword' => 'required|string|min:6|confirmed'
        );

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
           return \Redirect::back()->withErrors($validator)->withInput()->with('tabname', 'changepassword');
        }

        if (Auth::attempt(['email' => Auth::user()->email, 'password' => $request->oldPassword])) {
            $user = User::find(Auth::user()->id);
            $user->password = bcrypt($request->newPassword);
            $user->save();
        }else{
            $err = array("oldPassword" => "Old password is wrong.");
            return \Redirect::back()->withErrors($err)->withInput()->with('tabname', 'changepassword');
        }

        $text = __('words.successchangepassword');
        return redirect('profile')->with('message', array('text' => $text, 'status' => 'success'));
    }
}
