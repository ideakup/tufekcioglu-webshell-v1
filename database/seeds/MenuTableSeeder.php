<?php

use Illuminate\Database\Seeder;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menu')->delete();
        DB::table('menuvariable')->delete();

        DB::table('menu')->insert([
			'id' => 1,
			'top_id' => null,
			'name' => 'ANA SAYFA',
			'slug' => 'ana-sayfa',
			'description' => '',
			'order' => '1',
			'position' => 'top',
			'status' => 'active',
			'deleted' => 'no'
        ]);

        DB::table('menuvariable')->insert([
			'menu_id' => 1,
			'title' => 'ANA SAYFA',
			'headertheme' => 'home',
			'slidertype' => 'slider',
			'breadcrumbvisible' => 'no',
			'asidevisible' => 'no'
        ]);

        DB::table('menu')->insert([
			'id' => 2,
			'top_id' => null,
			'name' => 'HAKKIMIZDA',
			'slug' => 'hakkimizda',
			'description' => '',
			'order' => '2',
			'position' => 'top',
			'status' => 'active',
			'deleted' => 'no'
        ]);

        DB::table('menuvariable')->insert([
			'menu_id' => 2,
			'title' => 'HAKKIMIZDA',
			'headertheme' => 'general',
			'slidertype' => 'image',
			'breadcrumbvisible' => 'yes',
			'asidevisible' => 'yes'
        ]);

        DB::table('menu')->insert([
        	'id' => 3,
			'top_id' => null,
			'name' => 'ÜRÜNLER',
			'slug' => 'urunler',
			'description' => '',
			'order' => '3',
			'position' => 'top',
			'status' => 'active',
			'deleted' => 'no'
        ]);

        DB::table('menuvariable')->insert([
			'menu_id' => 3,
			'title' => 'ÜRÜNLER',
			'headertheme' => 'general',
			'slidertype' => 'image',
			'breadcrumbvisible' => 'yes',
			'asidevisible' => 'yes'
        ]);

        DB::table('menu')->insert([
        	'id' => 4,
			'top_id' => null,
			'name' => 'GALERİ',
			'slug' => 'galeri',
			'description' => '',
			'order' => '4',
			'position' => 'top',
			'status' => 'active',
			'deleted' => 'no'
        ]);

        DB::table('menuvariable')->insert([
			'menu_id' => 4,
			'title' => 'GALERİ',
			'headertheme' => 'general',
			'slidertype' => 'image',
			'breadcrumbvisible' => 'yes',
			'asidevisible' => 'yes'
        ]);

        DB::table('menu')->insert([
        	'id' => 5,
			'top_id' => null,
			'name' => 'LABORATUVAR',
			'slug' => 'laboratuvar',
			'description' => '',
			'order' => '5',
			'position' => 'top',
			'status' => 'active',
			'deleted' => 'no'
        ]);

        DB::table('menuvariable')->insert([
			'menu_id' => 5,
			'title' => 'LABORATUVAR',
			'headertheme' => 'general',
			'slidertype' => 'image',
			'breadcrumbvisible' => 'yes',
			'asidevisible' => 'yes'
        ]);

        DB::table('menu')->insert([
        	'id' => 6,
			'top_id' => null,
			'name' => 'BLOG',
			'slug' => 'blog',
			'description' => '',
			'order' => '6',
			'position' => 'top',
			'status' => 'active',
			'deleted' => 'no'
        ]);

        DB::table('menuvariable')->insert([
			'menu_id' => 6,
			'title' => 'BLOG',
			'headertheme' => 'general',
			'slidertype' => 'image',
			'breadcrumbvisible' => 'yes',
			'asidevisible' => 'yes'
        ]);

        DB::table('menu')->insert([
        	'id' => 7,
			'top_id' => null,
			'name' => 'İLETİŞİM',
			'slug' => 'iletisim',
			'description' => '',
			'order' => '7',
			'position' => 'top',
			'status' => 'active',
			'deleted' => 'no'
        ]);

        DB::table('menuvariable')->insert([
			'menu_id' => 7,
			'title' => 'İLETİŞİM',
			'headertheme' => 'general',
			'slidertype' => 'image',
			'breadcrumbvisible' => 'yes',
			'asidevisible' => 'yes'
        ]);
    }
}
