<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('top_content')->unsigned()->nullable();
            $table->enum('type', ['text', 'photo', 'photogallery', 'youtube', 'vimeo', 'link', 'file', 'audio', 'form', 'code', 'slide'])->default('text');
            $table->integer('order');
            $table->enum('status', ['active', 'passive'])->default('active');
            $table->enum('deleted', ['no', 'yes'])->default('no');
            $table->timestamps();
        });
          
        Schema::table('content', function (Blueprint $table) {
            $table->foreign('top_content')->references('id')->on('content');
        });
        
        Schema::create('contentvariable', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('content_id')->unsigned();
            $table->string('lang_code',6);

            $table->string('title', 190);
            $table->string('props', 190)->nullable();
            $table->enum('row', ['normal', 'full'])->default('normal');
            $table->enum('col', [1,2,3,4])->default(1);
            $table->integer('height')->nullable();
            $table->string('bgimageurl', 190)->nullable();
            $table->text('content')->nullable();

            $table->timestamps();
        });

        Schema::table('contentvariable', function (Blueprint $table) {
            $table->foreign('content_id')->references('id')->on('content');
        });

         Schema::create('menu_has_content', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('menu_id')->unsigned()->nullable();
            $table->integer('content_id')->unsigned()->nullable();
            $table->timestamps();
        });

        Schema::table('menu_has_content', function (Blueprint $table) {
            $table->foreign('menu_id')->references('id')->on('menu');
            $table->foreign('content_id')->references('id')->on('content');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content');
    }
}
 
